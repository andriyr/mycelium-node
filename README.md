# mycelium-node

Spiking neural network written in golang and based on the Izhikevich spiking neural network model.

## Requirements

    docker >= 17.12.0

## Getting Started

1. Checkout the project and the dependent submodules with the following command:

        git clone --recursive git@gitlab.com:andriyr/mycelium-node.git

2. Compile the protobuf files format. Change directory to /api and run:

    protoc --go_out=plugins=grpc:. *.proto

3. Start the node outside of docker with this command:

    go run main.go
