package server

import (
	"time"

	"../api"
    "../node/observer"
)

type NeuronSpikeEventStream struct {
    stream api.Node_StreamNeuronSpikeEventServer
	samplingInterval int
	last time.Time
	*observer.DefaultNeuronSpikeObserver
}

func (this *NeuronSpikeEventStream) handleNeuronSpikeEvent(event *observer.NeuronSpikeEvent) {
	now := time.Now()

	if now.Sub(this.last) >= time.Duration(this.samplingInterval) * time.Millisecond {
		neuronSpikeEvent := &api.NeuronSpikeEvent{NeuronId: event.NeuronId}
		this.stream.Send(neuronSpikeEvent)

		this.last = now
	}
}

func (this *NeuronSpikeEventStream) listen() {
    go func(this *NeuronSpikeEventStream) {
        neuronSpikeChannel := this.NeuronSpikeChannel()

        for {
            select {
            case neuronSpikeEvent := <- neuronSpikeChannel:
                this.handleNeuronSpikeEvent(neuronSpikeEvent)
            }
        }
    }(this)
}

func NewNeuronSpikeEventStream(stream api.Node_StreamNeuronSpikeEventServer, samplingInterval int) *NeuronSpikeEventStream {
    neuronSpikeEventStream := &NeuronSpikeEventStream{
		stream: stream,
		samplingInterval: samplingInterval,
		last: time.Now(),
		DefaultNeuronSpikeObserver: observer.NewDefaultNeuronSpikeObserver(),
	}

	neuronSpikeEventStream.listen()

	return neuronSpikeEventStream
}
