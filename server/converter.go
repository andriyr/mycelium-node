package server

import (
    "../api"
    "../node/neuron"
    "../node/synapse"
    "../node/preceptor"
    "../node/input"
    "../node/output"
    "../node/dopamine"
)

type Converter struct {}

func (this *Converter) ConvertNeuronCluster(neuronCluster *neuron.NeuronCluster) *api.NeuronClusterResponse {
    return &api.NeuronClusterResponse{
        Id: neuronCluster.Id(),
        Name: neuronCluster.Name(),
        Size: int32(neuronCluster.Size()),
        Ratio: neuronCluster.Ratio(),
        NeuronIds: neuronCluster.NeuronIds(),
        InputAdapterIds: neuronCluster.InputAdapterIds(),
        Graph: &api.Graph{X: neuronCluster.X(), Y: neuronCluster.Y()},
    }
}

func (this *Converter) ConvertSynapseCluster(synapseCluster *synapse.SynapseCluster) *api.SynapseClusterResponse {
    return &api.SynapseClusterResponse{
        Id: synapseCluster.Id(),
        Name: synapseCluster.Name(),
        NeuronsCovered: int32(synapseCluster.NeuronsCovered()),
        InitialSynapseWeight: synapseCluster.InitialSynapseWeight(),
        NeuronClusterIds: synapseCluster.NeuronClusterIds(),
        Graph: &api.Graph{X: synapseCluster.X(), Y: synapseCluster.Y()},
    }
}

func (this *Converter) ConvertSTDPPreceptor(stdpPreceptor *preceptor.STDPPreceptor) *api.STDPPreceptorResponse {
    return &api.STDPPreceptorResponse{
        Id: stdpPreceptor.Id(),
        Name: stdpPreceptor.Name(),
        MaxQueueSize: int32(stdpPreceptor.MaxQueueSize()),
        SynapseClusterId: stdpPreceptor.SynapseClusterId(),
        Graph: &api.Graph{X: stdpPreceptor.X(), Y: stdpPreceptor.Y()},
    }
}

func (this *Converter) ConvertModulatedSTDPPreceptor(modulatedSTDPPreceptor *preceptor.ModulatedSTDPPreceptor) *api.ModulatedSTDPPreceptorResponse {
    return &api.ModulatedSTDPPreceptorResponse{
        Id: modulatedSTDPPreceptor.Id(),
        Name: modulatedSTDPPreceptor.Name(),
        MaxQueueSize: int32(modulatedSTDPPreceptor.MaxQueueSize()),
        Sensitivity: modulatedSTDPPreceptor.Sensitivity(),
        SynapseClusterId: modulatedSTDPPreceptor.SynapseClusterId(),
        DopamineAdapterId: modulatedSTDPPreceptor.DopamineAdapterId(),
        Graph: &api.Graph{X: modulatedSTDPPreceptor.X(), Y: modulatedSTDPPreceptor.Y()},
    }
}

func (this *Converter) ConvertDopamineAdapter(dopamineAdapter *dopamine.DopamineAdapter) *api.DopamineAdapterResponse {
    return &api.DopamineAdapterResponse{
        Id: dopamineAdapter.Id(),
        Name: dopamineAdapter.Name(),
        Graph: &api.Graph{X: dopamineAdapter.X(), Y: dopamineAdapter.Y()},
    }
}

func (this *Converter) ConvertInputAdapter(inputAdapter *input.InputAdapter) *api.InputAdapterResponse {
    return &api.InputAdapterResponse{
        Id: inputAdapter.Id(),
        Name: inputAdapter.Name(),
        Size: int32(inputAdapter.Size()),
        EncodingWindow: int32(inputAdapter.EncodingWindow()),
        Graph: &api.Graph{X: inputAdapter.X(), Y: inputAdapter.Y()},
    }
}

func (this *Converter) ConvertOutputAdapter(outputAdapter *output.OutputAdapter) *api.OutputAdapterResponse {
    return &api.OutputAdapterResponse{
        Id: outputAdapter.Id(),
        Name: outputAdapter.Name(),
        Size: int32(outputAdapter.Size()),
        DecodingWindow: int32(outputAdapter.DecodingWindow()),
        NeuronClusterId: outputAdapter.NeuronClusterId(),
        Graph: &api.Graph{X: outputAdapter.X(), Y: outputAdapter.Y()},
    }
}

func NewConverter() *Converter {
    return &Converter{}
}
