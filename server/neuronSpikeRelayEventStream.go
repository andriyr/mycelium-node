package server

import (
	"time"

	"../api"
    "../node/observer"
)

type NeuronSpikeRelayEventStream struct {
    stream api.Node_StreamNeuronSpikeRelayEventServer
	samplingInterval int
	last time.Time
	*observer.DefaultNeuronSpikeRelayObserver
}

func (this *NeuronSpikeRelayEventStream) handleNeuronSpikeRelayEvent(event *observer.NeuronSpikeRelayEvent) {
    now := time.Now()

	if now.Sub(this.last) >= time.Duration(this.samplingInterval) * time.Millisecond {
		neuronSpikeRelayEvent := &api.NeuronSpikeRelayEvent{NeuronClusterId: event.NeuronClusterId, NeuronId: event.NeuronId, Potential: event.Potential}
		this.stream.Send(neuronSpikeRelayEvent)

		this.last = now
	}
}

func (this *NeuronSpikeRelayEventStream) listen() {
    go func(this *NeuronSpikeRelayEventStream) {
        neuronSpikeRelayChannel := this.NeuronSpikeRelayChannel()

        for {
            select {
            case neuronSpikeRelayEvent := <- neuronSpikeRelayChannel:
                this.handleNeuronSpikeRelayEvent(neuronSpikeRelayEvent)
            }
        }
    }(this)
}

func NewNeuronSpikeRelayEventStream(stream api.Node_StreamNeuronSpikeRelayEventServer, samplingInterval int) *NeuronSpikeRelayEventStream {
    neuronSpikeRelayEventStream := &NeuronSpikeRelayEventStream{
		stream: stream,
		samplingInterval: samplingInterval,
		last: time.Now(),
		DefaultNeuronSpikeRelayObserver: observer.NewDefaultNeuronSpikeRelayObserver(),
	}

	neuronSpikeRelayEventStream.listen()

	return neuronSpikeRelayEventStream
}
