package server

import (
	"io"

	"../api"
    "../node"

    "golang.org/x/net/context"
)

type Server struct {
    node *node.Node
	converter *Converter
}

func (this *Server) Node(ctx context.Context, request *api.NodeRequest) (*api.NodeResponse, error) {
	neuronClusters := make([]*api.NeuronClusterResponse, 0)
	for _, neuronCluster := range this.node.NeuronClusters() {
		neuronClusters = append(neuronClusters, this.converter.ConvertNeuronCluster(neuronCluster))
    }

	synapseClusters := make([]*api.SynapseClusterResponse, 0)
	for _, synapseCluster := range this.node.SynapseClusters() {
        synapseClusters = append(synapseClusters, this.converter.ConvertSynapseCluster(synapseCluster))
    }

	stdpPreceptors := make([]*api.STDPPreceptorResponse, 0)
	for _, stdpPreceptor := range this.node.STDPPreceptors() {
        stdpPreceptors = append(stdpPreceptors, this.converter.ConvertSTDPPreceptor(stdpPreceptor))
    }

	modulatedSTDPPreceptors := make([]*api.ModulatedSTDPPreceptorResponse, 0)
	for _, modulatedSTDPPreceptor := range this.node.ModulatedSTDPPreceptors() {
        modulatedSTDPPreceptors = append(modulatedSTDPPreceptors, this.converter.ConvertModulatedSTDPPreceptor(modulatedSTDPPreceptor))
    }

	dopamineAdapters := make([]*api.DopamineAdapterResponse, 0)
	for _, dopamineAdapter := range this.node.DopamineAdapters() {
        dopamineAdapters = append(dopamineAdapters, this.converter.ConvertDopamineAdapter(dopamineAdapter))
    }

	inputAdapters := make([]*api.InputAdapterResponse, 0)
	for _, inputAdapter := range this.node.InputAdapters() {
        inputAdapters = append(inputAdapters, this.converter.ConvertInputAdapter(inputAdapter))
    }

	outputAdapters := make([]*api.OutputAdapterResponse, 0)
	for _, outputAdapter := range this.node.OutputAdapters() {
        outputAdapters = append(outputAdapters, this.converter.ConvertOutputAdapter(outputAdapter))
    }

	return &api.NodeResponse{NeuronClusters: neuronClusters, SynapseClusters: synapseClusters, STDPPreceptors: stdpPreceptors, ModulatedSTDPPreceptors: modulatedSTDPPreceptors, DopamineAdapters: dopamineAdapters, InputAdapters: inputAdapters, OutputAdapters: outputAdapters}, nil
}

func (this *Server) CreateNeuronCluster(ctx context.Context, request *api.CreateNeuronClusterRequest) (*api.NeuronClusterResponse, error) {
	neuronCluster, err := this.node.CreateNeuronCluster(request.GetName(), int(request.GetSize()), request.GetRatio(), request.GetGraph().GetX(), request.GetGraph().GetY())

	if err != nil {
		return nil, err
	}

	return this.converter.ConvertNeuronCluster(neuronCluster), nil
}

func (this *Server) DeleteNeuronCluster(ctx context.Context, request *api.DeleteNeuronClusterRequest) (*api.DeleteNeuronClusterResponse, error) {
	this.node.DeleteNeuronCluster(request.GetId())

	return &api.DeleteNeuronClusterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) SetNeuronClusterPosition(ctx context.Context, request *api.SetNeuronClusterPositionRequest) (*api.SetNeuronClusterPositionResponse, error) {
	this.node.SetNeuronClusterPosition(request.GetId(), request.GetGraph().GetX(), request.GetGraph().GetY())

	return &api.SetNeuronClusterPositionResponse{Status:"SUCCESS"}, nil
}

func (this *Server) CreateSynapseCluster(ctx context.Context, request *api.CreateSynapseClusterRequest) (*api.SynapseClusterResponse, error) {
	synapseCluster, err := this.node.CreateSynapseCluster(request.GetName(), int(request.GetNeuronsCovered()), request.GetInitialSynapseWeight(), request.GetGraph().GetX(), request.GetGraph().GetY())

	if err != nil {
		return nil, err
	}

	return this.converter.ConvertSynapseCluster(synapseCluster), nil
}

func (this *Server) DeleteSynapseCluster(ctx context.Context, request *api.DeleteSynapseClusterRequest) (*api.DeleteSynapseClusterResponse, error) {
	this.node.DeleteSynapseCluster(request.GetId())

	return &api.DeleteSynapseClusterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) AttachSynapseCluster(ctx context.Context, request *api.AttachSynapseClusterRequest) (*api.AttachSynapseClusterResponse, error) {
	this.node.AttachSynapseCluster(request.GetSynapseClusterId(), request.GetNeuronClusterId())

	return &api.AttachSynapseClusterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) DetachSynapseCluster(ctx context.Context, request *api.DetachSynapseClusterRequest) (*api.DetachSynapseClusterResponse, error) {
	this.node.DetachSynapseCluster(request.GetSynapseClusterId(), request.GetNeuronClusterId())

	return &api.DetachSynapseClusterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) SetSynapseClusterPosition(ctx context.Context, request *api.SetSynapseClusterPositionRequest) (*api.SetSynapseClusterPositionResponse, error) {
	this.node.SetSynapseClusterPosition(request.GetId(), request.GetGraph().GetX(), request.GetGraph().GetY())

	return &api.SetSynapseClusterPositionResponse{Status:"SUCCESS"}, nil
}

func (this *Server) CreateSTDPPreceptor(ctx context.Context, request *api.CreateSTDPPreceptorRequest) (*api.STDPPreceptorResponse, error) {
	stdpPreceptor, err := this.node.CreateSTDPPreceptor(request.GetName(), int(request.GetMaxQueueSize()), request.GetGraph().GetX(), request.GetGraph().GetY())

	if err != nil {
		return nil, err
	}

	return this.converter.ConvertSTDPPreceptor(stdpPreceptor), nil
}

func (this *Server) DeleteSTDPPreceptor(ctx context.Context, request *api.DeleteSTDPPreceptorRequest) (*api.DeleteSTDPPreceptorResponse, error) {
	this.node.DeleteSTDPPreceptor(request.GetId())

	return &api.DeleteSTDPPreceptorResponse{Status: "SUCCESS"}, nil
}

func (this *Server) AttachSTDPPreceptor(ctx context.Context, request *api.AttachSTDPPreceptorRequest) (*api.AttachSTDPPreceptorResponse, error) {
	this.node.AttachSTDPPreceptor(request.GetSTDPPreceptorId(), request.GetSynapseClusterId())

	return &api.AttachSTDPPreceptorResponse{Status: "SUCCESS"}, nil
}

func (this *Server) DetachSTDPPreceptor(ctx context.Context, request *api.DetachSTDPPreceptorRequest) (*api.DetachSTDPPreceptorResponse, error) {
	this.node.DetachSTDPPreceptor(request.GetSTDPPreceptorId())

	return &api.DetachSTDPPreceptorResponse{Status: "SUCCESS"}, nil
}

func (this *Server) SetSTDPPreceptorPosition(ctx context.Context, request *api.SetSTDPPreceptorPositionRequest) (*api.SetSTDPPreceptorPositionResponse, error) {
	this.node.SetSTDPPreceptorPosition(request.GetId(), request.GetGraph().GetX(), request.GetGraph().GetY())

	return &api.SetSTDPPreceptorPositionResponse{Status:"SUCCESS"}, nil
}

func (this *Server) CreateModulatedSTDPPreceptor(ctx context.Context, request *api.CreateModulatedSTDPPreceptorRequest) (*api.ModulatedSTDPPreceptorResponse, error) {
	modulatedSTDPPreceptor, err := this.node.CreateModulatedSTDPPreceptor(request.GetName(), int(request.GetMaxQueueSize()), request.GetSensitivity(), request.GetGraph().GetX(), request.GetGraph().GetY())

	if err != nil {
		return nil, err
	}

	return this.converter.ConvertModulatedSTDPPreceptor(modulatedSTDPPreceptor), nil
}

func (this *Server) DeleteModulatedSTDPPreceptor(ctx context.Context, request *api.DeleteModulatedSTDPPreceptorRequest) (*api.DeleteModulatedSTDPPreceptorResponse, error) {
	this.node.DeleteModulatedSTDPPreceptor(request.GetId())

	return &api.DeleteModulatedSTDPPreceptorResponse{Status: "SUCCESS"}, nil
}

func (this *Server) AttachModulatedSTDPPreceptor(ctx context.Context, request *api.AttachModulatedSTDPPreceptorRequest) (*api.AttachModulatedSTDPPreceptorResponse, error) {
	this.node.AttachModulatedSTDPPreceptor(request.GetModulatedSTDPPreceptorId(), request.GetSynapseClusterId())

	return &api.AttachModulatedSTDPPreceptorResponse{Status: "SUCCESS"}, nil
}

func (this *Server) DetachModulatedSTDPPreceptor(ctx context.Context, request *api.DetachModulatedSTDPPreceptorRequest) (*api.DetachModulatedSTDPPreceptorResponse, error) {
	this.node.DetachModulatedSTDPPreceptor(request.GetModulatedSTDPPreceptorId())

	return &api.DetachModulatedSTDPPreceptorResponse{Status: "SUCCESS"}, nil
}

func (this *Server) SetModulatedSTDPPreceptorPosition(ctx context.Context, request *api.SetModulatedSTDPPreceptorPositionRequest) (*api.SetModulatedSTDPPreceptorPositionResponse, error) {
	this.node.SetModulatedSTDPPreceptorPosition(request.GetId(), request.GetGraph().GetX(), request.GetGraph().GetY())

	return &api.SetModulatedSTDPPreceptorPositionResponse{Status:"SUCCESS"}, nil
}

func (this *Server) CreateInputAdapter(ctx context.Context, request *api.CreateInputAdapterRequest) (*api.InputAdapterResponse, error) {
	inputAdapter, err := this.node.CreateInputAdapter(request.GetName(), int(request.GetSize()), int(request.GetEncodingWindow()), request.GetGraph().GetX(), request.GetGraph().GetY())

	if err != nil {
		return nil, err
	}

	return this.converter.ConvertInputAdapter(inputAdapter), nil
}

func (this *Server) DeleteInputAdapter(ctx context.Context, request *api.DeleteInputAdapterRequest) (*api.DeleteInputAdapterResponse, error) {
	this.node.DeleteInputAdapter(request.GetId())

	return &api.DeleteInputAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) AttachInputAdapter(ctx context.Context, request *api.AttachInputAdapterRequest) (*api.AttachInputAdapterResponse, error) {
	this.node.AttachInputAdapter(request.GetInputAdapterId(), request.GetNeuronClusterId())

	return &api.AttachInputAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) DetachInputAdapter(ctx context.Context, request *api.DetachInputAdapterRequest) (*api.DetachInputAdapterResponse, error) {
	this.node.DetachInputAdapter(request.GetInputAdapterId(), request.GetNeuronClusterId())

	return &api.DetachInputAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) SetInputAdapterPosition(ctx context.Context, request *api.SetInputAdapterPositionRequest) (*api.SetInputAdapterPositionResponse, error) {
	this.node.SetInputAdapterPosition(request.GetId(), request.GetGraph().GetX(), request.GetGraph().GetY())

	return &api.SetInputAdapterPositionResponse{Status:"SUCCESS"}, nil
}

func (this *Server) CreateOutputAdapter(ctx context.Context, request *api.CreateOutputAdapterRequest) (*api.OutputAdapterResponse, error) {
	outputAdapter, err := this.node.CreateOutputAdapter(request.GetName(), int(request.GetSize()), int(request.GetDecodingWindow()), request.GetGraph().GetX(), request.GetGraph().GetY())

	if err != nil {
		return nil, err
	}

	return this.converter.ConvertOutputAdapter(outputAdapter), nil
}

func (this *Server) DeleteOutputAdapter(ctx context.Context, request *api.DeleteOutputAdapterRequest) (*api.DeleteOutputAdapterResponse, error) {
	this.node.DeleteOutputAdapter(request.GetId())

	return &api.DeleteOutputAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) AttachOutputAdapter(ctx context.Context, request *api.AttachOutputAdapterRequest) (*api.AttachOutputAdapterResponse, error) {
	this.node.AttachOutputAdapter(request.GetOutputAdapterId(), request.GetNeuronClusterId())

	return &api.AttachOutputAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) DetachOutputAdapter(ctx context.Context, request *api.DetachOutputAdapterRequest) (*api.DetachOutputAdapterResponse, error) {
	this.node.DetachOutputAdapter(request.GetOutputAdapterId())

	return &api.DetachOutputAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) SetOutputAdapterPosition(ctx context.Context, request *api.SetOutputAdapterPositionRequest) (*api.SetOutputAdapterPositionResponse, error) {
	this.node.SetOutputAdapterPosition(request.GetId(), request.GetGraph().GetX(), request.GetGraph().GetY())

	return &api.SetOutputAdapterPositionResponse{Status:"SUCCESS"}, nil
}

func (this *Server) CreateDopamineAdapter(ctx context.Context, request *api.CreateDopamineAdapterRequest) (*api.DopamineAdapterResponse, error) {
	dopamineAdapter, err := this.node.CreateDopamineAdapter(request.GetName(), request.GetGraph().GetX(), request.GetGraph().GetY())

	if err != nil {
		return nil, err
	}

	return this.converter.ConvertDopamineAdapter(dopamineAdapter), nil
}

func (this *Server) DeleteDopamineAdapter(ctx context.Context, request *api.DeleteDopamineAdapterRequest) (*api.DeleteDopamineAdapterResponse, error) {
	this.node.DeleteDopamineAdapter(request.GetId())

	return &api.DeleteDopamineAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) AttachDopamineAdapter(ctx context.Context, request *api.AttachDopamineAdapterRequest) (*api.AttachDopamineAdapterResponse, error) {
	this.node.AttachDopamineAdapter(request.GetDopamineAdapterId(), request.GetModulatedSTDPPreceptorId())

	return &api.AttachDopamineAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) DetachDopamineAdapter(ctx context.Context, request *api.DetachDopamineAdapterRequest) (*api.DetachDopamineAdapterResponse, error) {
	this.node.DetachDopamineAdapter(request.GetDopamineAdapterId(), request.GetModulatedSTDPPreceptorId())

	return &api.DetachDopamineAdapterResponse{Status: "SUCCESS"}, nil
}

func (this *Server) SetDopamineAdapterPosition(ctx context.Context, request *api.SetDopamineAdapterPositionRequest) (*api.SetDopamineAdapterPositionResponse, error) {
	this.node.SetDopamineAdapterPosition(request.GetId(), request.GetGraph().GetX(), request.GetGraph().GetY())

	return &api.SetDopamineAdapterPositionResponse{Status:"SUCCESS"}, nil
}

func (this *Server) StreamInput(stream api.Node_StreamInputServer) error {
	for {
		request, err := stream.Recv()

		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		this.node.StreamInput(request.GetInputAdapterId(), request.GetValues())
	}

	return nil
}

func (this *Server) StreamOutput(request *api.StreamOutputRequest, stream api.Node_StreamOutputServer) error {
	return this.node.StreamOutput(request.GetOutputAdapterId(), NewOutputEventStream(stream))
}

func (this *Server) StreamDopamine(stream api.Node_StreamDopamineServer) error {
	for {
		request, err := stream.Recv()

		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		this.node.StreamDopamine(request.GetDopamineAdapterId(), request.GetDopamine())
	}

	return nil
}

func (this *Server) StreamNeuronSpikeEvent(request *api.StreamNeuronSpikeEventRequest, stream api.Node_StreamNeuronSpikeEventServer) error {
	return this.node.StreamNeuronSpikeEvent(request.GetId(), NewNeuronSpikeEventStream(stream, int(request.GetSamplingInterval())))
}

func (this *Server) StreamNeuronSpikeRelayEvent(request *api.StreamNeuronSpikeRelayEventRequest, stream api.Node_StreamNeuronSpikeRelayEventServer) error {
	return this.node.StreamNeuronSpikeRelayEvent(request.GetId(), NewNeuronSpikeRelayEventStream(stream, int(request.GetSamplingInterval())))
}

func (this *Server) StreamSynapseWeightUpdateEvent(request *api.StreamSynapseWeightUpdateEventRequest, stream api.Node_StreamSynapseWeightUpdateEventServer) error {
	return this.node.StreamSynapseWeightUpdateEvent(request.GetId(), NewSynapseWeightUpdateEventStream(stream, int(request.GetSamplingInterval())))
}

func NewServer(node *node.Node) *Server {
    return &Server{node: node}
}
