package server

import (
	"time"

	"../api"
    "../node/observer"
)

type SynapseWeightUpdateEventStream struct {
    stream api.Node_StreamSynapseWeightUpdateEventServer
	samplingInterval int
	last time.Time
	*observer.DefaultSynapseWeightUpdateObserver
}

func (this *SynapseWeightUpdateEventStream) handleSynapseWeightUpdateEvent(event *observer.SynapseWeightUpdateEvent) {
    now := time.Now()

	if now.Sub(this.last) >= time.Duration(this.samplingInterval) * time.Millisecond {
		synapseWeightUpdateEvent := &api.SynapseWeightUpdateEvent{PreSynapticNeuronId: event.PreSynapticNeuronId, PostSynapticNeuronId: event.PostSynapticNeuronId, WeightDelta: event.WeightDelta}
		this.stream.Send(synapseWeightUpdateEvent)

		this.last = now
	}
}

func (this *SynapseWeightUpdateEventStream) listen() {
    go func(this *SynapseWeightUpdateEventStream) {
        synapseWeightUpdateChannel := this.SynapseWeightUpdateChannel()

        for {
            select {
            case synapseWeightUpdateEvent := <- synapseWeightUpdateChannel:
                this.handleSynapseWeightUpdateEvent(synapseWeightUpdateEvent)
            }
        }
    }(this)
}

func NewSynapseWeightUpdateEventStream(stream api.Node_StreamSynapseWeightUpdateEventServer, samplingInterval int) *SynapseWeightUpdateEventStream {
    synapseWeightUpdateObserver := &SynapseWeightUpdateEventStream{
		stream: stream,
		samplingInterval: samplingInterval,
		last: time.Now(),
		DefaultSynapseWeightUpdateObserver: observer.NewDefaultSynapseWeightUpdateObserver(),
	}

	synapseWeightUpdateObserver.listen()

	return synapseWeightUpdateObserver
}
