package server

import (
	"../api"
    "../node/observer"
)

type OutputEventStream struct {
    stream api.Node_StreamOutputServer
	*observer.DefaultOutputObserver
}

func (this *OutputEventStream) handleOutputEvent(event *observer.OutputEvent) {
    this.stream.Send(&api.StreamOutputResponse{
    	OutputAdapterId: event.OutputAdapterId,
        Values: event.Values,
    })
}

func (this *OutputEventStream) listen() {
    go func(this *OutputEventStream) {
        outputChannel := this.OutputChannel()

        for {
            select {
            case outputEvent := <- outputChannel:
                this.handleOutputEvent(outputEvent)
            }
        }
    }(this)
}

func NewOutputEventStream(stream api.Node_StreamOutputServer) *OutputEventStream {
    outputEventStream := &OutputEventStream{
		stream: stream,
		DefaultOutputObserver: observer.NewDefaultOutputObserver(),
	}

	outputEventStream.listen()

	return outputEventStream
}
