package preceptor

import (
    "math"
    "time"

    "../synapse"
    "../dopamine"
    "../observer"
    "../graph"

    "github.com/satori/go.uuid"
)

type SynapticTag struct {
    value float64
    time int64
}

type ModulatedSTDPPreceptor struct {
    id string
    name string
    maxQueueSize int
    queue *Queue
    sensitivity float64
    synapticTags map[string]map[string]*SynapticTag
    synapseCluster *synapse.SynapseCluster
    dopamineAdapter *dopamine.DopamineAdapter
    *observer.DefaultNeuronSpikeObserver
    *observer.DefaultDopamineObserver
    *observer.DefaultSynapseWeightUpdateSubject
    *graph.Graph
}

func (this *ModulatedSTDPPreceptor) Id() string {
    return this.id
}

func (this *ModulatedSTDPPreceptor) Name() string {
    return this.name
}

func (this *ModulatedSTDPPreceptor) MaxQueueSize() int {
    return this.maxQueueSize
}

func (this *ModulatedSTDPPreceptor) Sensitivity() float64 {
    return this.sensitivity
}

func (this *ModulatedSTDPPreceptor) SynapseClusterId() string {
    if this.synapseCluster != nil {
        return this.synapseCluster.Id()
    }
    return ""
}

func (this *ModulatedSTDPPreceptor) DopamineAdapterId() string {
    if this.dopamineAdapter != nil {
        return this.dopamineAdapter.Id()
    }
    return ""
}

func (this *ModulatedSTDPPreceptor) SetSynapseCluster(synapseCluster *synapse.SynapseCluster) {
    this.synapseCluster = synapseCluster
    synapseCluster.AttachNeuronSpikeObserver(this)
    this.AttachSynapseWeightUpdateObserver(synapseCluster)
}

func (this *ModulatedSTDPPreceptor) UnsetSynapseCluster() {
    if this.synapseCluster != nil {
        this.synapseCluster.DetachNeuronSpikeObserver(this)
        this.DetachSynapseWeightUpdateObserver(this.synapseCluster)
        this.synapseCluster = nil
    }
}

func (this *ModulatedSTDPPreceptor) SetDopamineAdapter(dopamineAdapter *dopamine.DopamineAdapter) {
    this.dopamineAdapter = dopamineAdapter
    dopamineAdapter.AttachDopamineObserver(this)
}

func (this *ModulatedSTDPPreceptor) UnsetDopamineAdapter() {
    if this.dopamineAdapter != nil {
        this.dopamineAdapter.DetachDopamineObserver(this)
        this.dopamineAdapter = nil
    }
}

func (this *ModulatedSTDPPreceptor) stdp(firstSpikeTime, secondSpikeTime int64) float64 {
    timeDelta := float64(firstSpikeTime - secondSpikeTime)
    // TODO as part of settings
    longTermPotentiationTau := float64(15)
    longTermPotentiationLimit := float64(0.0004)
    longTermDepressionTau := float64(20)
    longTermDepressionLimit := float64(0.0004)

    if timeDelta >= 0 {
        return longTermPotentiationLimit*float64(math.Exp(-timeDelta/float64(time.Millisecond)*longTermPotentiationTau))
    } else {
        return -longTermDepressionLimit*float64(math.Exp(timeDelta/float64(time.Millisecond)*longTermDepressionTau))
    }
}

func (this *ModulatedSTDPPreceptor) calculateSynapticTag(firstSpikeEvent *observer.NeuronSpikeEvent, secondSpikeEvent *observer.NeuronSpikeEvent) {
    now := time.Now().UnixNano()

    if _, exists := this.synapticTags[firstSpikeEvent.NeuronId]; !exists {
        this.synapticTags[firstSpikeEvent.NeuronId]= make(map[string]*SynapticTag)
    }

    if synapticTag, exists := this.synapticTags[firstSpikeEvent.NeuronId][secondSpikeEvent.NeuronId]; !exists {
        this.synapticTags[firstSpikeEvent.NeuronId][secondSpikeEvent.NeuronId] = &SynapticTag{value: 0, time: now}
    } else {
        value := synapticTag.value - synapticTag.value/float64((now - synapticTag.time)/int64(time.Second)) + this.stdp(firstSpikeEvent.Time, secondSpikeEvent.Time)
        this.synapticTags[firstSpikeEvent.NeuronId][secondSpikeEvent.NeuronId] = &SynapticTag{value: value, time: now}
    }
}

func (this *ModulatedSTDPPreceptor) handleNeuronSpikeEvent(event *observer.NeuronSpikeEvent) {
    this.queue.Push(event)

    if this.maxQueueSize < this.queue.Len() {
        oldestSpikeEvent := this.queue.Poll()

        for i := 0; i < this.queue.Len(); i++ {
            nextSpikeEvent := this.queue.PeekAt(i)

            if oldestSpikeEvent.NeuronId != nextSpikeEvent.NeuronId {
                this.calculateSynapticTag(oldestSpikeEvent, nextSpikeEvent)
                this.calculateSynapticTag(nextSpikeEvent, oldestSpikeEvent)
            }
        }
    }
}

func (this *ModulatedSTDPPreceptor) handleDopamineEvent(event *observer.DopamineEvent) {
    now := time.Now().UnixNano()

    for preSynapticNeuronId, synapticTags := range this.synapticTags {
        for postSynapticNeuronId, synapticTag := range synapticTags {
            value := synapticTag.value - synapticTag.value/float64((now - synapticTag.time)/int64(time.Second))
            this.synapticTags[preSynapticNeuronId][postSynapticNeuronId] = &SynapticTag{value: value, time: now}

            this.NotifySynapseWeightUpdateEvent(&observer.SynapseWeightUpdateEvent{
                PreSynapticNeuronId: preSynapticNeuronId,
                PostSynapticNeuronId: postSynapticNeuronId,
                WeightDelta: synapticTag.value * event.Dopamine * this.sensitivity,
            })
        }
    }
}

func (this *ModulatedSTDPPreceptor) listen() {
    neuronSpikeChannel := this.NeuronSpikeChannel()
    dopamineChannel := this.DopamineChannel()

    go func() {
        for {
            select {
            case neuronSpikeEvent := <- neuronSpikeChannel:
                this.handleNeuronSpikeEvent(neuronSpikeEvent)
            case dopamineEvent := <- dopamineChannel:
                this.handleDopamineEvent(dopamineEvent)
            }
        }
    }()
}

func NewModulatedSTDPPreceptor(name string, maxQueueSize int, sensitivity, x, y float64) *ModulatedSTDPPreceptor {
    modulatedSTDPPreceptor := &ModulatedSTDPPreceptor{
        id: uuid.Must(uuid.NewV4()).String(),
        name: name,
        maxQueueSize: maxQueueSize,
        queue: NewQueue(),
        sensitivity: sensitivity,
        synapticTags: make(map[string]map[string]*SynapticTag),
        DefaultNeuronSpikeObserver: observer.NewDefaultNeuronSpikeObserver(),
        DefaultDopamineObserver: observer.NewDefaultDopamineObserver(),
        DefaultSynapseWeightUpdateSubject: observer.NewDefaultSynapseWeightUpdateSubject(),
        Graph: graph.NewGraph(x, y),
    }

    modulatedSTDPPreceptor.listen()

    return modulatedSTDPPreceptor
}
