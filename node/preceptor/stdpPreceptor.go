package preceptor

import (
    "math"
    "time"

    "../synapse"
    "../observer"
    "../graph"

    "github.com/satori/go.uuid"
)

type STDPPreceptor struct {
    id string
    name string
    maxQueueSize int
    queue *Queue
    synapseCluster *synapse.SynapseCluster
    *observer.DefaultNeuronSpikeObserver
    *observer.DefaultSynapseWeightUpdateSubject
    *graph.Graph
}

func (this *STDPPreceptor) Id() string {
    return this.id
}

func (this *STDPPreceptor) Name() string {
    return this.name
}

func (this *STDPPreceptor) MaxQueueSize() int {
    return this.maxQueueSize
}

func (this *STDPPreceptor) SynapseClusterId() string {
    if this.synapseCluster != nil {
        return this.synapseCluster.Id()
    }
    return ""
}

func (this *STDPPreceptor) SetSynapseCluster(synapseCluster *synapse.SynapseCluster) {
    this.synapseCluster = synapseCluster
    synapseCluster.AttachNeuronSpikeObserver(this)
    this.AttachSynapseWeightUpdateObserver(synapseCluster)
}

func (this *STDPPreceptor) UnsetSynapseCluster() {
    if this.synapseCluster != nil {
        this.synapseCluster.DetachNeuronSpikeObserver(this)
        this.DetachSynapseWeightUpdateObserver(this.synapseCluster)
        this.synapseCluster = nil
    }
}

func (this *STDPPreceptor) stdp(firstSpikeTime, secondSpikeTime int64) float64 {
    timeDelta := float64(firstSpikeTime - secondSpikeTime)
    // TODO as part of settings
    longTermPotentiationTau := float64(15)
    longTermPotentiationLimit := float64(0.0004)
    longTermDepressionTau := float64(20)
    longTermDepressionLimit := float64(0.0004)

    if timeDelta >= 0 {
        return longTermPotentiationLimit*float64(math.Exp(-timeDelta/float64(time.Millisecond)*longTermPotentiationTau))
    } else {
        return -longTermDepressionLimit*float64(math.Exp(timeDelta/float64(time.Millisecond)*longTermDepressionTau))
    }
}

func (this *STDPPreceptor) differentiateEvents(firstSpikeEvent *observer.NeuronSpikeEvent, secondSpikeEvent *observer.NeuronSpikeEvent) {
    this.NotifySynapseWeightUpdateEvent(&observer.SynapseWeightUpdateEvent{
        PreSynapticNeuronId: firstSpikeEvent.NeuronId,
        PostSynapticNeuronId: secondSpikeEvent.NeuronId,
        WeightDelta: this.stdp(firstSpikeEvent.Time, secondSpikeEvent.Time),
    })
}

func (this *STDPPreceptor) handleNeuronSpikeEvent(event *observer.NeuronSpikeEvent) {
    this.queue.Push(event)

    if this.maxQueueSize < this.queue.Len() {
        oldestSpikeEvent := this.queue.Poll()

        for i := 0; i < this.queue.Len(); i++ {
            nextSpikeEvent := this.queue.PeekAt(i)

            if oldestSpikeEvent.NeuronId != nextSpikeEvent.NeuronId {
                this.differentiateEvents(oldestSpikeEvent, nextSpikeEvent)
                this.differentiateEvents(nextSpikeEvent, oldestSpikeEvent)
            }
        }
    }
}

func (this *STDPPreceptor) listen() {
    go func() {
        neuronSpikeChannel := this.NeuronSpikeChannel()

        for {
            select {
            case neuronSpikeEvent := <- neuronSpikeChannel:
                this.handleNeuronSpikeEvent(neuronSpikeEvent)
            }
        }
    }()
}

func NewSTDPPreceptor(name string, maxQueueSize int, x, y float64) *STDPPreceptor {
    stdpPreceptor := &STDPPreceptor{
        id: uuid.Must(uuid.NewV4()).String(),
        name: name,
        maxQueueSize: maxQueueSize,
        queue: NewQueue(),
        DefaultNeuronSpikeObserver: observer.NewDefaultNeuronSpikeObserver(),
        DefaultSynapseWeightUpdateSubject: observer.NewDefaultSynapseWeightUpdateSubject(),
        Graph: graph.NewGraph(x, y),
    }

    stdpPreceptor.listen()

    return stdpPreceptor
}
