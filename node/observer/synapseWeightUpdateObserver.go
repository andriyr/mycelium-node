package observer

import (
    "sync"
)

type SynapseWeightUpdateObserver interface {
    SynapseWeightUpdateChannel() chan *SynapseWeightUpdateEvent
}

type DefaultSynapseWeightUpdateObserver struct {
    channel chan *SynapseWeightUpdateEvent
}

func (this *DefaultSynapseWeightUpdateObserver) SynapseWeightUpdateChannel() chan *SynapseWeightUpdateEvent {
    return this.channel
}

func NewDefaultSynapseWeightUpdateObserver() *DefaultSynapseWeightUpdateObserver {
    return &DefaultSynapseWeightUpdateObserver{
        channel: make(chan *SynapseWeightUpdateEvent, 100),
    }
}

type SynapseWeightUpdateSubject interface {
    AttachSynapseWeightUpdateObserver(observer SynapseWeightUpdateObserver)
    DetachSynapseWeightUpdateObserver(observer SynapseWeightUpdateObserver)
    HasSynapseWeightUpdateObserver(observer SynapseWeightUpdateObserver)
    NotifySynapseWeightUpdateEvent(event *SynapseWeightUpdateEvent)
}

type DefaultSynapseWeightUpdateSubject struct {
    channels map[SynapseWeightUpdateObserver]chan *SynapseWeightUpdateEvent
    mux sync.Mutex
}

func (this *DefaultSynapseWeightUpdateSubject) AttachSynapseWeightUpdateObserver(observer SynapseWeightUpdateObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    this.channels[observer] = observer.SynapseWeightUpdateChannel()
}

func (this *DefaultSynapseWeightUpdateSubject) DetachSynapseWeightUpdateObserver(observer SynapseWeightUpdateObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    delete(this.channels, observer)
}

func (this *DefaultSynapseWeightUpdateSubject) HasSynapseWeightUpdateObserver(observer SynapseWeightUpdateObserver) bool {
    if _, exists := this.channels[observer]; !exists {
        return false
    }
    return true
}

func (this *DefaultSynapseWeightUpdateSubject) NotifySynapseWeightUpdateEvent(event *SynapseWeightUpdateEvent) {
    this.mux.Lock()
    defer this.mux.Unlock()
    for _, channel := range this.channels {
        event := event

        select{
        case channel <- event:
        default: // TODO Error handling here
        }
    }
}

func NewDefaultSynapseWeightUpdateSubject() *DefaultSynapseWeightUpdateSubject {
	return &DefaultSynapseWeightUpdateSubject{
        channels: make(map[SynapseWeightUpdateObserver]chan *SynapseWeightUpdateEvent),
    }
}
