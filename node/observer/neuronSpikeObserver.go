package observer

import (
    "sync"
)

type NeuronSpikeObserver interface {
    NeuronSpikeChannel() chan *NeuronSpikeEvent
}

type DefaultNeuronSpikeObserver struct {
    channel chan *NeuronSpikeEvent
}

func (this *DefaultNeuronSpikeObserver) NeuronSpikeChannel() chan *NeuronSpikeEvent {
    return this.channel
}

func NewDefaultNeuronSpikeObserver() *DefaultNeuronSpikeObserver {
    return &DefaultNeuronSpikeObserver{
        channel: make(chan *NeuronSpikeEvent, 100),
    }
}

type NeuronSpikeSubject interface {
    AttachNeuronSpikeObserver(observer NeuronSpikeObserver)
    DetachNeuronSpikeObserver(observer NeuronSpikeObserver)
    HasNeuronSpikeObserver(observer NeuronSpikeObserver) bool
    NotifyNeuronSpikeEvent(event *NeuronSpikeEvent)
}

type DefaultNeuronSpikeSubject struct {
    channels map[NeuronSpikeObserver]chan *NeuronSpikeEvent
    mux sync.Mutex
}

func (this *DefaultNeuronSpikeSubject) AttachNeuronSpikeObserver(observer NeuronSpikeObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    this.channels[observer] = observer.NeuronSpikeChannel()
}

func (this *DefaultNeuronSpikeSubject) DetachNeuronSpikeObserver(observer NeuronSpikeObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    delete(this.channels, observer)
}

func (this *DefaultNeuronSpikeSubject) HasNeuronSpikeObserver(observer NeuronSpikeObserver) bool {
    if _, exists := this.channels[observer]; !exists {
        return false
    }
    return true
}

func (this *DefaultNeuronSpikeSubject) NotifyNeuronSpikeEvent(event *NeuronSpikeEvent) {
    this.mux.Lock()
    defer this.mux.Unlock()
    for _, channel := range this.channels {
        event := event

        select{
        case channel <- event:
        default: // TODO Error handling here
        }
    }
}

func NewDefaultNeuronSpikeSubject() *DefaultNeuronSpikeSubject {
	return &DefaultNeuronSpikeSubject{
        channels: make(map[NeuronSpikeObserver]chan *NeuronSpikeEvent),
    }
}
