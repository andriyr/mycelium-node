package observer

import (
    "sync"
)

type InputObserver interface {
    InputChannel() chan *InputEvent
}

type DefaultInputObserver struct {
    channel chan *InputEvent
}

func (this *DefaultInputObserver) InputChannel() chan *InputEvent {
    return this.channel
}

func NewDefaultInputObserver() *DefaultInputObserver {
    return &DefaultInputObserver{
        channel: make(chan *InputEvent, 100),
    }
}

type InputSubject interface {
    AttachInputObserver(observer InputObserver)
    DetachInputObserver(observer InputObserver)
    HasInputObserver(observer InputObserver) bool
    NotifyInputEvent(event *InputEvent)
}

type DefaultInputSubject struct {
    channels map[InputObserver]chan *InputEvent
    mux sync.Mutex
}

func (this *DefaultInputSubject) AttachInputObserver(observer InputObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    this.channels[observer] = observer.InputChannel()
}

func (this *DefaultInputSubject) DetachInputObserver(observer InputObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    delete(this.channels, observer)
}

func (this *DefaultInputSubject) HasInputObserver(observer InputObserver) bool {
    if _, exists := this.channels[observer]; !exists {
        return false
    }
    return true
}

func (this *DefaultInputSubject) NotifyInputEvent(event *InputEvent) {
    this.mux.Lock()
    defer this.mux.Unlock()
    for _, channel := range this.channels {
        event := event

        select{
        case channel <- event:
        default: // TODO Error handling here
        }
    }
}

func NewDefaultInputSubject() *DefaultInputSubject {
	return &DefaultInputSubject{
        channels: make(map[InputObserver]chan *InputEvent),
    }
}
