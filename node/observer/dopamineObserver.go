package observer

import (
    "sync"
)

type DopamineObserver interface {
    DopamineChannel() chan *DopamineEvent
}

type DefaultDopamineObserver struct {
    channel chan *DopamineEvent
}

func (this *DefaultDopamineObserver) DopamineChannel() chan *DopamineEvent {
    return this.channel
}

func NewDefaultDopamineObserver() *DefaultDopamineObserver {
    return &DefaultDopamineObserver{
        channel: make(chan *DopamineEvent, 100),
    }
}

type DopamineSubject interface {
    AttachDopamineObserver(observer DopamineObserver)
    DetachDopamineObserver(observer DopamineObserver)
    HasDopamineObserver(observer DopamineObserver) bool
    NotifyDopamineEvent(event *DopamineEvent)
}

type DefaultDopamineSubject struct {
    channels map[DopamineObserver]chan *DopamineEvent
    mux sync.Mutex
}

func (this *DefaultDopamineSubject) AttachDopamineObserver(observer DopamineObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    this.channels[observer] = observer.DopamineChannel()
}

func (this *DefaultDopamineSubject) DetachDopamineObserver(observer DopamineObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    delete(this.channels, observer)
}

func (this *DefaultDopamineSubject) HasDopamineObserver(observer DopamineObserver) bool {
    if _, exists := this.channels[observer]; !exists {
        return false
    }
    return true
}

func (this *DefaultDopamineSubject) NotifyDopamineEvent(event *DopamineEvent) {
    this.mux.Lock()
    defer this.mux.Unlock()
    for _, channel := range this.channels {
        event := event

        select{
        case channel <- event:
        default: // TODO Dopamine handling here
        }
    }
}

func NewDefaultDopamineSubject() *DefaultDopamineSubject {
	return &DefaultDopamineSubject{
        channels: make(map[DopamineObserver]chan *DopamineEvent),
    }
}
