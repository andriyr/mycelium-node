package observer

import (
    "sync"
)

type OutputObserver interface {
    OutputChannel() chan *OutputEvent
}

type DefaultOutputObserver struct {
    channel chan *OutputEvent
}

func (this *DefaultOutputObserver) OutputChannel() chan *OutputEvent {
    return this.channel
}

func NewDefaultOutputObserver() *DefaultOutputObserver {
    return &DefaultOutputObserver{
        channel: make(chan *OutputEvent, 100),
    }
}

type OutputSubject interface {
    AttachOutputObserver(observer OutputObserver)
    DetachOutputObserver(observer OutputObserver)
    HasOutputObserver(observer OutputObserver) bool
    NotifyOutputEvent(event *OutputEvent)
}

type DefaultOutputSubject struct {
    channels map[OutputObserver]chan *OutputEvent
    mux sync.Mutex
}

func (this *DefaultOutputSubject) AttachOutputObserver(observer OutputObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    this.channels[observer] = observer.OutputChannel()
}

func (this *DefaultOutputSubject) DetachOutputObserver(observer OutputObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    delete(this.channels, observer)
}

func (this *DefaultOutputSubject) HasOutputObserver(observer OutputObserver) bool {
    if _, exists := this.channels[observer]; !exists {
        return false
    }
    return true
}

func (this *DefaultOutputSubject) NotifyOutputEvent(event *OutputEvent) {
    this.mux.Lock()
    defer this.mux.Unlock()
    for _, channel := range this.channels {
        event := event

        select{
        case channel <- event:
        default: // TODO Error handling here
        }
    }
}

func NewDefaultOutputSubject() *DefaultOutputSubject {
	return &DefaultOutputSubject{
        channels: make(map[OutputObserver]chan *OutputEvent),
    }
}
