package observer

// TODO Make immutable
type InputEvent struct {
	InputAdapterId string
 	InputId string
}

type OutputEvent struct {
	OutputAdapterId string
	Values []float64
}

type NeuronSpikeEvent struct {
    NeuronId string
	Time int64
	Potential float64
}

type NeuronSpikeRelayEvent struct {
	SynapseId string
	NeuronClusterId string
	NeuronId string
	Potential float64
}

type SynapseWeightUpdateEvent struct {
	PreSynapticNeuronId string
	PostSynapticNeuronId string
	WeightDelta float64
}

type DopamineEvent struct {
	DopamineAdapterId string
	Dopamine float64
}
