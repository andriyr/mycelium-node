package observer

import (
    "sync"
)

type NeuronSpikeRelayObserver interface {
    NeuronSpikeRelayChannel() chan *NeuronSpikeRelayEvent
}

type DefaultNeuronSpikeRelayObserver struct {
    channel chan *NeuronSpikeRelayEvent
}

func (this *DefaultNeuronSpikeRelayObserver) NeuronSpikeRelayChannel() chan *NeuronSpikeRelayEvent {
    return this.channel
}

func NewDefaultNeuronSpikeRelayObserver() *DefaultNeuronSpikeRelayObserver {
    return &DefaultNeuronSpikeRelayObserver{
        channel: make(chan *NeuronSpikeRelayEvent, 100),
    }
}

type NeuronSpikeRelaySubject interface {
    AttachNeuronSpikeRelayObserver(observer NeuronSpikeRelayObserver)
    DetachNeuronSpikeRelayObserver(observer NeuronSpikeRelayObserver)
    HasNeuronSpikeRelayObserver(observer NeuronSpikeRelayObserver) bool
    NotifyNeuronSpikeRelayEvent(event *NeuronSpikeRelayEvent)
}

type DefaultNeuronSpikeRelaySubject struct {
    channels map[NeuronSpikeRelayObserver]chan *NeuronSpikeRelayEvent
    mux sync.Mutex
}

func (this *DefaultNeuronSpikeRelaySubject) AttachNeuronSpikeRelayObserver(observer NeuronSpikeRelayObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    this.channels[observer] = observer.NeuronSpikeRelayChannel()
}

func (this *DefaultNeuronSpikeRelaySubject) DetachNeuronSpikeRelayObserver(observer NeuronSpikeRelayObserver) {
    this.mux.Lock()
    defer this.mux.Unlock()
    delete(this.channels, observer)
}

func (this *DefaultNeuronSpikeRelaySubject) HasNeuronSpikeRelayObserver(observer NeuronSpikeRelayObserver) bool {
    if _, exists := this.channels[observer]; !exists {
        return false
    }
    return true
}

func (this *DefaultNeuronSpikeRelaySubject) NotifyNeuronSpikeRelayEvent(event *NeuronSpikeRelayEvent) {
    this.mux.Lock()
    defer this.mux.Unlock()
    for _, channel := range this.channels {
        event := event

        select{
        case channel <- event:
        default: // TODO Error handling here
        }
    }
}

func NewDefaultNeuronSpikeRelaySubject() *DefaultNeuronSpikeRelaySubject {
	return &DefaultNeuronSpikeRelaySubject{
        channels: make(map[NeuronSpikeRelayObserver]chan *NeuronSpikeRelayEvent),
    }
}
