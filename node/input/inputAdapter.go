package input

import (
    "time"
    "sort"

    "../observer"
    "../graph"
    "../util"

    "github.com/satori/go.uuid"
)

type InputAdapter struct {
    id string
    name string
    size int
    encodingWindow int // NOTE encoding window in milliseconds
    inputIds []string
    *observer.DefaultInputSubject
    *graph.Graph
}

func (this *InputAdapter) Id() string {
    return this.id
}

func (this *InputAdapter) Name() string {
    return this.name
}

func (this *InputAdapter) Size() int {
    return this.size
}

func (this *InputAdapter) EncodingWindow() int {
    return this.encodingWindow
}

func (this *InputAdapter) InputIds() []string {
    return this.inputIds
}

func (this *InputAdapter) Send(values []float64) {
    go func(values []float64, encodingWindow int) {
        spikeOrder := util.NewFloat64Slice(values)
        sort.Sort(spikeOrder)

        for _, idx := range spikeOrder.Idx() {
            time.Sleep(time.Duration(int64(values[idx] * float64(encodingWindow) * 1000000)) * time.Nanosecond)
            this.NotifyInputEvent(&observer.InputEvent{InputAdapterId: this.id, InputId: this.inputIds[idx]})
        }
    }(values, this.encodingWindow)
}

func NewInputAdapter(name string, size, encodingWindow int, x, y float64) *InputAdapter {
    inputIds := make([]string, 0)

    for i := 0; i < size; i++ {
        inputIds = append(inputIds, uuid.Must(uuid.NewV4()).String())
	}

    inputAdapter := &InputAdapter{
        id: uuid.Must(uuid.NewV4()).String(),
        name: name,
        size: size,
        encodingWindow: encodingWindow,
        inputIds: inputIds,
        DefaultInputSubject: observer.NewDefaultInputSubject(),
        Graph: graph.NewGraph(x, y),
    }

    return inputAdapter
}
