package util

import (
	"sort"
)

type Slice struct {
	sort.Interface
	idx []int
}

func (s Slice) Swap(i, j int) {
	s.Interface.Swap(i, j)
	s.idx[i], s.idx[j] = s.idx[j], s.idx[i]
}

func (s Slice) Idx() []int {
	return s.idx
}

func NewSlice(n sort.Interface) *Slice {
	s := &Slice{Interface: n, idx: make([]int, n.Len())}
	for i := range s.idx {
		s.idx[i] = i
	}
	return s
}

func NewIntSlice(n []int) *Slice {
    return NewSlice(sort.IntSlice(n))
}

func NewFloat64Slice(n []float64) *Slice {
    return NewSlice(sort.Float64Slice(n))
}

func NewStringSlice(n []string) *Slice {
    return NewSlice(sort.StringSlice(n))
}
