package neuron

import (
    "math"
    "math/rand"
    "time"

    "github.com/satori/go.uuid"
)

const (
    Excitory = "Excitory"
    Inhibitory = "Inhibitory"
)

type Neuron interface {
    Id() string
    Potential() float64
    AddPotential(potential float64)
}

type ExcitoryNeuron struct {
    id string
    potential float64 // v
    recovery float64 // u
    threshold float64 // default: 30mV
    recoveryFactor float64 // a - default: 0.02
    sensitivity float64 // b - default: 0.2
    potentialReset  float64 // c - default: -65V
    recoveryReset float64 // d - default: 2
    lastUpdated int64
    onSpike func(string, int64, float64)
}

func (this *ExcitoryNeuron) Id() string {
    return this.id
}

func (this *ExcitoryNeuron) Potential() float64 {
    return this.potential
}

func (this *ExcitoryNeuron) AddPotential(potential float64) {
    currentTime := time.Now().UnixNano()

    delta := currentTime - this.lastUpdated
    this.lastUpdated = currentTime

    // TODO Might be wrong
    for i := 0; i < int(delta / int64(time.Millisecond)); i++ {
        recovery := this.recoveryFactor*(this.sensitivity*this.potential - this.recovery)
        potential := 0.04*this.potential*this.potential + 5*this.potential + 140 - this.recovery
        if (this.recovery - recovery) < 0.1 && (this.potential - potential) < 0.1 {
            break
        }
        this.recovery += recovery
        this.potential += potential
	}

    this.potential += potential

    if this.potential > this.threshold {
        this.potential = this.potentialReset
        this.recovery += this.recoveryReset

        this.onSpike(this.id, currentTime, 1)
    }
}

func NewExcitoryNeuron(onSpike func(string, int64, float64)) *ExcitoryNeuron {
    return &ExcitoryNeuron{
        id: uuid.Must(uuid.NewV4()).String(),
        potential: -65,
        recovery: -65*0.2,
        threshold: 30,
        recoveryFactor: 0.02,
        sensitivity: 0.2,
        potentialReset: -65+15*float64(math.Pow(2, rand.Float64())),
        recoveryReset: 8-6*rand.Float64(),
        onSpike: onSpike,
    }
}

type InhibitoryNeuron struct {
    id string
    potential float64 // v
    recovery float64 // u
    threshold float64 // default: 30mV
    recoveryFactor float64 // a - default: 0.02
    sensitivity float64 // b - default: 0.2
    potentialReset  float64 // c - default: -65V
    recoveryReset float64 // d - default: 2
    lastUpdated int64
    onSpike func(string, int64, float64)
}

func (this *InhibitoryNeuron) Id() string {
    return this.id
}

func (this *InhibitoryNeuron) Potential() float64 {
    return this.potential
}

func (this *InhibitoryNeuron) AddPotential(potential float64) {
    currentTime := time.Now().UnixNano()

    delta := currentTime - this.lastUpdated
    this.lastUpdated = currentTime

    // TODO might we wrong
    for i := 0; i < int(delta / int64(time.Millisecond)); i++ {
        recovery := this.recoveryFactor*(this.sensitivity*this.potential - this.recovery)
        potential := 0.04*this.potential*this.potential + 5*this.potential + 140 - this.recovery
        if (this.recovery - recovery) < 0.1 && (this.potential - potential) < 0.1 {
            break
        }
        this.recovery += recovery
        this.potential += potential
	}

    this.potential += potential

    if this.potential > this.threshold {
        this.potential = this.potentialReset
        this.recovery += this.recoveryReset

        this.onSpike(this.id, currentTime, -1)
    }
}

func NewInhibitoryNeuron(onSpike func(string, int64, float64)) *InhibitoryNeuron {
    return &InhibitoryNeuron{
        id: uuid.Must(uuid.NewV4()).String(),
        potential: -65,
        recovery: -65*0.2,
        threshold: 30,
        recoveryFactor: 0.02+0.08*rand.Float64(),
        sensitivity: 0.25-0.0*rand.Float64(),
        potentialReset: -65,
        recoveryReset: 2,
        onSpike: onSpike,
    }
}
