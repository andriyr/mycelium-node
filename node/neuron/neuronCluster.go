package neuron

import (
    "math/rand"

    "../observer"
    "../graph"
    "../input"

    "github.com/satori/go.uuid"
)

type NeuronCluster struct {
    id string
    name string
    size int
    ratio float64
    neurons map[string]Neuron
    inputAdapters map[string]*input.InputAdapter
    inputs map[string]map[string]Neuron
    neuronSpikeChannel chan *observer.NeuronSpikeEvent
    *observer.DefaultInputObserver
    *observer.DefaultNeuronSpikeRelayObserver
    *observer.DefaultNeuronSpikeSubject
    *graph.Graph
}

func (this *NeuronCluster) Id() string {
    return this.id
}

func (this *NeuronCluster) Name() string {
    return this.name
}

func (this *NeuronCluster) Size() int {
    return this.size
}

func (this *NeuronCluster) Ratio() float64 {
    return this.ratio
}

func (this *NeuronCluster) NeuronIds() []string {
    neuronIds := make([]string, 0)

    for _, neuron := range this.neurons {
        neuronIds = append(neuronIds, neuron.Id())
    }

    return neuronIds
}

func (this *NeuronCluster) addPotential(neuronId string, potential float64) {
    this.neurons[neuronId].AddPotential(potential)
}

func (this *NeuronCluster) AddInputAdapter(inputAdapter *input.InputAdapter) {
    this.inputAdapters[inputAdapter.Id()] = inputAdapter

    if this.inputs[inputAdapter.Id()] == nil {
        this.inputs[inputAdapter.Id()] = make(map[string]Neuron)
    }

    for _, inputId := range inputAdapter.InputIds() {
        // TODO check the neuron is not used by anything else
        this.inputs[inputAdapter.Id()][inputId] = this.neurons[this.NeuronIds()[rand.Intn(this.size)]]
    }

    inputAdapter.AttachInputObserver(this)
}

func (this *NeuronCluster) RemoveInputAdapter(id string) {
    if inputAdapter, exists := this.inputAdapters[id]; exists {
        inputAdapter.DetachInputObserver(this)
        delete(this.inputAdapters, id)
        delete(this.inputs, id)
    }
}

func (this *NeuronCluster) InputAdapterIds() []string {
    inputAdapterIds := make([]string, 0)

    for _, inputAdapter := range this.inputAdapters {
        inputAdapterIds = append(inputAdapterIds, inputAdapter.Id())
    }

    return inputAdapterIds
}

func (this *NeuronCluster) addNeuron(neuron Neuron) {
    this.neurons[neuron.Id()] = neuron
}

func (this *NeuronCluster) onSpike(neuronId string, spikeTime int64, potential float64) {
    this.NotifyNeuronSpikeEvent(&observer.NeuronSpikeEvent{
        NeuronId: neuronId,
        Time: spikeTime,
        Potential: potential,
    })
}

func (this *NeuronCluster) handleInputEvent(event *observer.InputEvent) {
    neuron := this.inputs[event.InputAdapterId][event.InputId]
    neuron.AddPotential(100)
}

func (this *NeuronCluster) handleNeuronSpikeRelayEvent(event *observer.NeuronSpikeRelayEvent) {
    if event.NeuronClusterId == this.id {
        neuron := this.neurons[event.NeuronId]
        neuron.AddPotential(event.Potential)
    }
}

func (this *NeuronCluster) listen() {
    go func(this *NeuronCluster) {
        inputChannel := this.InputChannel()
        neuronSpikeRelayChannel := this.NeuronSpikeRelayChannel()

        for {
            select {
            case inputEvent := <- inputChannel:
                this.handleInputEvent(inputEvent)
            case neuronSpikeRelayEvent := <- neuronSpikeRelayChannel:
                this.handleNeuronSpikeRelayEvent(neuronSpikeRelayEvent)
            }
        }
    }(this)
}

func NewNeuronCluster(name string, size int, ratio, x, y float64) *NeuronCluster {
    neuronCluster := &NeuronCluster{
        id: uuid.Must(uuid.NewV4()).String(),
        name: name,
        size: size,
        ratio: ratio,
        neurons: make(map[string]Neuron),
        inputAdapters: make(map[string]*input.InputAdapter),
        inputs: make(map[string]map[string]Neuron),
        DefaultInputObserver: observer.NewDefaultInputObserver(),
        DefaultNeuronSpikeRelayObserver: observer.NewDefaultNeuronSpikeRelayObserver(),
        DefaultNeuronSpikeSubject: observer.NewDefaultNeuronSpikeSubject(),
        Graph: graph.NewGraph(x, y),
    }

    for i := 0; i < size; i++ {
        if ratio > rand.Float64() {
            neuronCluster.addNeuron(NewExcitoryNeuron(neuronCluster.onSpike))
        } else {
            neuronCluster.addNeuron(NewInhibitoryNeuron(neuronCluster.onSpike))
        }
	}

    neuronCluster.listen()

    return neuronCluster
}
