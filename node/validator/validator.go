package validator

import (
    "errors"
)

type NodeValidator struct {}

func(this *NodeValidator) ValidateCreateNeuronCluster(name string, size int, ratio, x, y float64) error {
    if len(name) < 1 {
        return errors.New("A neuron cluster must have a name.")
    }
    if size < 1 {
        return errors.New("A neuron cluster must not have a size smaller than 1")
    }
    if ratio < 0 {
        return errors.New("A neuron cluster ratio must not be smaller than 0 ")
    }
    if ratio > 1 {
        return errors.New("A neuron cluster ratio must not be greater than 1")
    }
    return nil
}

func (this *NodeValidator) ValidateCreateSynapseCluster(name string, neuronsCovered int, initialSynapseWeight, x, y float64) error {
    if len(name) < 1 {
        return errors.New("A synapse cluster must have a name.")
    }
    if neuronsCovered < 1 {
        return errors.New("A synapse cluster must not have less than 1 synapses per neuron")
    }
    if initialSynapseWeight < 0 {
        return errors.New("A synapse cluster initial weight must be greater than 0")
    }
    return nil
}

func (this *NodeValidator) ValidateCreateSTDPPreceptor(name string, maxQueueSize int, x, y float64) error {
    if len(name) < 1 {
        return errors.New("An STDP preceptor must have a name.")
    }
    if maxQueueSize > 0 {
        return errors.New("An STDP preceptor must have a queue size greater than 0")
    }
    return nil
}

func (this *NodeValidator) ValidateCreateModulatedSTDPPreceptor(name string, maxQueueSize int, sensitivity, x, y float64) error {
    if len(name) < 1 {
        return errors.New("A modulated STDP preceptor must have a name.")
    }
    if maxQueueSize < 1 {
        return errors.New("An STDP preceptor must have a queue size greater than 0")
    }
    if sensitivity <= 0 {
        return errors.New("A modulated STDP preceptor must have sensitivity smaller than 0")
    }
    return nil
}

func (this *NodeValidator) ValidateCreateInputAdapter(name string, size, encodingWindow int, x, y float64) error {
    if len(name) < 1 {
        return errors.New("An input adapter must have a name.")
    }
    if size < 1 {
        return errors.New("An input adapter must not have a size smaller than 1")
    }
    if encodingWindow < 1 {
        return errors.New("An input adapter must not have an encoding window smaller than 1")
    }
    return nil
}

func (this *NodeValidator) ValidateCreateOutputAdapter(name string, size, decodingWindow int, x, y float64) error {
    if len(name) < 1 {
        return errors.New("An output adapter must have a name.")
    }
    if size < 1 {
        return errors.New("An input adapter must not have a size smaller than 1")
    }
    if decodingWindow < 1 {
        return errors.New("An input adapter must not have an encoding window smaller than 1")
    }
    return nil
}

func (this *NodeValidator) ValidateCreateDopamineAdapter(name string, x, y float64) error {
    if len(name) < 1 {
        return errors.New("An dopamine adapter must have a name.")
    }
    return nil
}

func NewNodeValidator() *NodeValidator {
    return &NodeValidator{}
}
