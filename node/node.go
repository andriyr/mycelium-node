package node

import (
    "time"
    "errors"

    "./validator"
    "./neuron"
    "./synapse"
    "./preceptor"
    "./input"
    "./output"
    "./dopamine"
    "./stream"
)

type Node struct {
    neuronClusters map[string]*neuron.NeuronCluster
    synapseClusters map[string]*synapse.SynapseCluster
    stdpPreceptors map[string]*preceptor.STDPPreceptor
    modulatedSTDPPreceptors map[string]*preceptor.ModulatedSTDPPreceptor
    inputAdapters map[string]*input.InputAdapter
    outputAdapters map[string]*output.OutputAdapter
    dopamineAdapters map[string]*dopamine.DopamineAdapter
    validator *validator.NodeValidator
}

func (this *Node) NeuronClusters() []*neuron.NeuronCluster {
    neuronClusters := make([]*neuron.NeuronCluster, 0)

    for _, neuronCluster := range this.neuronClusters {
        neuronClusters = append(neuronClusters, neuronCluster)
    }

    return neuronClusters
}

func (this *Node) CreateNeuronCluster(name string, size int, ratio, x, y float64) (*neuron.NeuronCluster, error) {
    err := this.validator.ValidateCreateNeuronCluster(name, size, ratio, x, y)

    if err != nil {
        return nil, err
    }

    neuronCluster := neuron.NewNeuronCluster(name, size, ratio, x, y)
    this.neuronClusters[neuronCluster.Id()] = neuronCluster
    return neuronCluster, nil
}

func (this *Node) DeleteNeuronCluster(neuronClusterId string) {
    for _, synapseCluster := range this.synapseClusters {
        synapseCluster.RemoveNeuronCluster(neuronClusterId)
    }

    for _, outputAdapter := range this.outputAdapters {
        if outputAdapter.NeuronClusterId() == neuronClusterId {
            outputAdapter.UnsetNeuronCluster()
        }
    }

    delete(this.neuronClusters, neuronClusterId)
}

func (this *Node) SetNeuronClusterPosition(neuronClusterId string, x, y float64) error {
    if neuronCluster, exists := this.neuronClusters[neuronClusterId]; exists {
        neuronCluster.SetPosition(x, y)
        return nil
    }
    return errors.New("Neuron Cluster Not Found")
}

func (this *Node) SynapseClusters() []*synapse.SynapseCluster {
    synapseClusters := make([]*synapse.SynapseCluster, 0)

    for _, synapseCluster := range this.synapseClusters {
        synapseClusters = append(synapseClusters, synapseCluster)
    }

    return synapseClusters
}

func (this *Node) CreateSynapseCluster(name string, neuronsCovered int, initialSynapseWeight float64, x, y float64) (*synapse.SynapseCluster, error) {
    err := this.validator.ValidateCreateSynapseCluster(name, neuronsCovered, initialSynapseWeight, x, y)

    if err != nil {
        return nil, err
    }

    synapseCluster := synapse.NewSynapseCluster(name, neuronsCovered, initialSynapseWeight, x, y)
    this.synapseClusters[synapseCluster.Id()] = synapseCluster
    return synapseCluster, nil
}

func (this *Node) DeleteSynapseCluster(synapseClusterId string) {
    for _, stdpPreceptor := range this.stdpPreceptors {
        if stdpPreceptor.SynapseClusterId() == synapseClusterId {
            stdpPreceptor.UnsetSynapseCluster()
        }
    }

    for _, modulatedSTDPPreceptor := range this.modulatedSTDPPreceptors {
        if modulatedSTDPPreceptor.SynapseClusterId() == synapseClusterId {
            modulatedSTDPPreceptor.UnsetSynapseCluster()
        }
    }

    delete(this.synapseClusters, synapseClusterId)
}

func (this *Node) AttachSynapseCluster(synapseClusterId, neuronClusterId string) error {
    if _, exists := this.synapseClusters[synapseClusterId]; !exists {
        return errors.New("Synapse Cluster Not Found")
    }

    if _, exists := this.neuronClusters[neuronClusterId]; !exists {
        return errors.New("Neuron Cluster Not Found")
    }

    this.synapseClusters[synapseClusterId].AddNeuronCluster(this.neuronClusters[neuronClusterId])
    return nil
}

func (this *Node) DetachSynapseCluster(synapseClusterId, neuronClusterId string) error {
    if _, exists := this.synapseClusters[synapseClusterId]; !exists {
        return errors.New("Synapse Cluster Not Found")
    }

    this.synapseClusters[synapseClusterId].RemoveNeuronCluster(neuronClusterId)
    return nil
}

func (this *Node) SetSynapseClusterPosition(synapseClusterId string, x, y float64) error {
    if synapseCluster, exists := this.synapseClusters[synapseClusterId]; exists {
        synapseCluster.SetPosition(x, y)
        return nil
    }
    return errors.New("Synapse Cluster Not Found")
}

func (this *Node) STDPPreceptors() []*preceptor.STDPPreceptor {
    stdpPreceptors := make([]*preceptor.STDPPreceptor, 0)

    for _, stdpPreceptor := range this.stdpPreceptors {
        stdpPreceptors = append(stdpPreceptors, stdpPreceptor)
    }

    return stdpPreceptors
}

func (this *Node) CreateSTDPPreceptor(name string, maxQueueSize int, x, y float64) (*preceptor.STDPPreceptor, error) {
    err := this.validator.ValidateCreateSTDPPreceptor(name, maxQueueSize, x, y)

    if err != nil {
        return nil, err
    }

    stdpPreceptor := preceptor.NewSTDPPreceptor(name, maxQueueSize, x, y)
    this.stdpPreceptors[stdpPreceptor.Id()] = stdpPreceptor
    return stdpPreceptor, nil
}

func (this *Node) DeleteSTDPPreceptor(stdpPreceptorId string) {
    delete(this.stdpPreceptors, stdpPreceptorId)
}

func (this *Node) AttachSTDPPreceptor(stdpPreceptorId, synapseClusterId string) error {
    if _, exists := this.stdpPreceptors[stdpPreceptorId]; !exists {
        return errors.New("STDP Learning Not Found")
    }

    if _, exists := this.synapseClusters[synapseClusterId]; !exists {
        return errors.New("Synapse Cluster Not Found")
    }

    this.stdpPreceptors[stdpPreceptorId].SetSynapseCluster(this.synapseClusters[synapseClusterId])
    return nil
}

func (this *Node) DetachSTDPPreceptor(stdpPreceptorId string) error {
    if _, exists := this.stdpPreceptors[stdpPreceptorId]; !exists {
        return errors.New("STDP Learning Not Found")
    }

    this.stdpPreceptors[stdpPreceptorId].UnsetSynapseCluster()
    return nil
}

func (this *Node) SetSTDPPreceptorPosition(stdpPreceptorId string, x, y float64) error {
    if stdpPreceptor, exists := this.stdpPreceptors[stdpPreceptorId]; exists {
        stdpPreceptor.SetPosition(x, y)
        return nil
    }
    return errors.New("STDP Learning Not Found")
}

func (this *Node) ModulatedSTDPPreceptors() []*preceptor.ModulatedSTDPPreceptor {
    modulatedSTDPPreceptors := make([]*preceptor.ModulatedSTDPPreceptor, 0)

    for _, modulatedSTDPPreceptor := range this.modulatedSTDPPreceptors {
        modulatedSTDPPreceptors = append(modulatedSTDPPreceptors, modulatedSTDPPreceptor)
    }

    return modulatedSTDPPreceptors
}

func (this *Node) CreateModulatedSTDPPreceptor(name string, maxQueueSize int, sensitivity, x, y float64) (*preceptor.ModulatedSTDPPreceptor, error) {
    err := this.validator.ValidateCreateModulatedSTDPPreceptor(name, maxQueueSize, sensitivity, x, y)

    if err != nil {
        return nil, err
    }

    modulatedSTDPPreceptor := preceptor.NewModulatedSTDPPreceptor(name, maxQueueSize, sensitivity, x, y)
    this.modulatedSTDPPreceptors[modulatedSTDPPreceptor.Id()] = modulatedSTDPPreceptor
    return modulatedSTDPPreceptor, nil
}

func (this *Node) DeleteModulatedSTDPPreceptor(modulatedSTDPPreceptorId string) {
    delete(this.modulatedSTDPPreceptors, modulatedSTDPPreceptorId)
}

func (this *Node) AttachModulatedSTDPPreceptor(modulatedSTDPPreceptorId, synapseClusterId string) error {
    if _, exists := this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId]; !exists {
        return errors.New("Dopamine Preceptor Not Found")
    }

    if _, exists := this.synapseClusters[synapseClusterId]; !exists {
        return errors.New("Synapse Cluster Not Found")
    }

    this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId].SetSynapseCluster(this.synapseClusters[synapseClusterId])
    return nil
}

func (this *Node) DetachModulatedSTDPPreceptor(modulatedSTDPPreceptorId string) error {
    if _, exists := this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId]; !exists {
        return errors.New("Dopamine Preceptor Not Found")
    }

    this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId].UnsetSynapseCluster()
    this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId].UnsetDopamineAdapter()
    return nil
}

func (this *Node) SetModulatedSTDPPreceptorPosition(modulatedSTDPPreceptorId string, x, y float64) error {
    if modulatedSTDPPreceptor, exists := this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId]; exists {
        modulatedSTDPPreceptor.SetPosition(x, y)
        return nil
    }
    return errors.New("Dopamine Preceptor Not Found")
}

func (this *Node) InputAdapters() []*input.InputAdapter {
    inputAdapters := make([]*input.InputAdapter, 0)

    for _, inputAdapter := range this.inputAdapters {
        inputAdapters = append(inputAdapters, inputAdapter)
    }

    return inputAdapters
}

func (this *Node) CreateInputAdapter(name string, size, encodingWindow int, x, y float64) (*input.InputAdapter, error) {
    err := this.validator.ValidateCreateInputAdapter(name, size, encodingWindow, x, y)

    if err != nil {
        return nil, err
    }

    inputAdapter := input.NewInputAdapter(name, size, encodingWindow, x, y)
    this.inputAdapters[inputAdapter.Id()] = inputAdapter
    return inputAdapter, nil
}

func (this *Node) DeleteInputAdapter(inputAdapterId string) {
    for _, neuronCluster := range this.neuronClusters {
        neuronCluster.RemoveInputAdapter(inputAdapterId)
    }

    delete(this.inputAdapters, inputAdapterId)
}

func (this *Node) AttachInputAdapter(inputAdapterId, neuronClusterId string) error {
    if _, exists := this.inputAdapters[inputAdapterId]; !exists {
        return errors.New("Input Adapter Not Found")
    }

    if _, exists := this.neuronClusters[neuronClusterId]; !exists {
        return errors.New("Neuron Cluster Not Found")
    }

    this.neuronClusters[neuronClusterId].AddInputAdapter(this.inputAdapters[inputAdapterId])
    return nil
}

func (this *Node) DetachInputAdapter(inputAdapterId, neuronClusterId string) error {
    if _, exists := this.inputAdapters[inputAdapterId]; !exists {
        return errors.New("Input Adapter Not Found")
    }

    if _, exists := this.neuronClusters[neuronClusterId]; !exists {
        return errors.New("Neuron Cluster Not Found")
    }

    this.neuronClusters[neuronClusterId].RemoveInputAdapter(inputAdapterId)
    return nil
}

func (this *Node) SetInputAdapterPosition(inputAdapterId string, x, y float64) error {
    if inputAdapter, exists := this.inputAdapters[inputAdapterId]; exists {
        inputAdapter.SetPosition(x, y)
        return nil
    }
    return errors.New("Input Adapter Not Found")
}

func (this *Node) OutputAdapters() []*output.OutputAdapter {
    outputAdapters := make([]*output.OutputAdapter, 0)

    for _, outputAdapter := range this.outputAdapters {
        outputAdapters = append(outputAdapters, outputAdapter)
    }

    return outputAdapters
}

func (this *Node) CreateOutputAdapter(name string, size, decodingWindow int, x, y float64) (*output.OutputAdapter, error) {
    err := this.validator.ValidateCreateOutputAdapter(name, size, decodingWindow, x, y)

    if err != nil {
        return nil, err
    }

    outputAdapter := output.NewOutputAdapter(name, size, decodingWindow, x, y)
    this.outputAdapters[outputAdapter.Id()] = outputAdapter
    return outputAdapter, nil
}

func (this *Node) DeleteOutputAdapter(outputAdapterId string) {
    delete(this.outputAdapters, outputAdapterId)
}

func (this *Node) AttachOutputAdapter(outputAdapterId, neuronClusterId string) error {
    if _, exists := this.outputAdapters[outputAdapterId]; !exists {
        return errors.New("Output Adaper Not Found")
    }

    if _, exists := this.neuronClusters[neuronClusterId]; !exists {
        return errors.New("Neuron Cluster Not Found")
    }

    this.outputAdapters[outputAdapterId].SetNeuronCluster(this.neuronClusters[neuronClusterId])
    return nil
}

func (this *Node) DetachOutputAdapter(outputAdapterId string) error {
    if _, exists := this.outputAdapters[outputAdapterId]; !exists {
        return errors.New("Output Adapter Not Found")
    }

    this.outputAdapters[outputAdapterId].UnsetNeuronCluster()
    return nil
}

func (this *Node) SetOutputAdapterPosition(outputAdapterId string, x, y float64) error {
    if outputAdapter, exists := this.outputAdapters[outputAdapterId]; exists {
        outputAdapter.SetPosition(x, y)
        return nil
    }
    return errors.New("Output Adapter Not Found")
}

func (this *Node) DopamineAdapters() []*dopamine.DopamineAdapter {
    dopamineAdapters := make([]*dopamine.DopamineAdapter, 0)

    for _, dopamineAdapter := range this.dopamineAdapters {
        dopamineAdapters = append(dopamineAdapters, dopamineAdapter)
    }

    return dopamineAdapters
}

func (this *Node) CreateDopamineAdapter(name string, x, y float64) (*dopamine.DopamineAdapter, error) {
    err := this.validator.ValidateCreateDopamineAdapter(name, x, y)

    if err != nil {
        return nil, err
    }

    dopamineAdapter := dopamine.NewDopamineAdapter(name, x, y)
    this.dopamineAdapters[dopamineAdapter.Id()] = dopamineAdapter
    return dopamineAdapter, nil
}

func (this *Node) DeleteDopamineAdapter(dopamineAdapterId string) {
    for _, modulatedSTDPPreceptor := range this.modulatedSTDPPreceptors {
        if modulatedSTDPPreceptor.DopamineAdapterId() == dopamineAdapterId {
            modulatedSTDPPreceptor.UnsetDopamineAdapter()
        }
    }

    delete(this.dopamineAdapters, dopamineAdapterId)
}

func (this *Node) AttachDopamineAdapter(dopamineAdapterId, modulatedSTDPPreceptorId string) error {
    if _, exists := this.dopamineAdapters[dopamineAdapterId]; !exists {
        return errors.New("Dopamine Adapter Not Found")
    }

    if _, exists := this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId]; !exists {
        return errors.New("Modulated STDPP Preceptor Not Found")
    }

    this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId].SetDopamineAdapter(this.dopamineAdapters[dopamineAdapterId])
    return nil
}

func (this *Node) DetachDopamineAdapter(dopamineAdapterId, modulatedSTDPPreceptorId string) error {
    if _, exists := this.dopamineAdapters[dopamineAdapterId]; !exists {
        return errors.New("Dopamine Adapter Not Found")
    }

    if _, exists := this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId]; !exists {
        return errors.New("Modulated STDPP Preceptor Not Found")
    }

    this.modulatedSTDPPreceptors[modulatedSTDPPreceptorId].UnsetDopamineAdapter()
    return nil
}

func (this *Node) SetDopamineAdapterPosition(dopamineAdapterId string, x, y float64) error {
    if dopamineAdapter, exists := this.dopamineAdapters[dopamineAdapterId]; exists {
        dopamineAdapter.SetPosition(x, y)
        return nil
    }
    return errors.New("Dopamine Adapter Not Found")
}

func (this *Node) StreamInput(inputAdapterId string, values []float64) error {
    if inputAdapter, exists := this.inputAdapters[inputAdapterId]; exists {
        inputAdapter.Send(values)
        return nil
    } else {
        return errors.New("Input Adapter Not Found")
    }
}

func (this *Node) StreamOutput(outputAdapterId string, stream stream.OutputStream) error {
    if _, exists := this.outputAdapters[outputAdapterId]; !exists {
        return errors.New("Output Adapter Not Found")
    }

    this.outputAdapters[outputAdapterId].AttachOutputObserver(stream)
    // TODO Run in goroutine?
    for {
        time.Sleep(time.Second * 10)
        if outputAdapter, exists := this.outputAdapters[outputAdapterId]; exists {
            if !outputAdapter.HasOutputObserver(stream) {
                return nil
            }
        } else {
            return nil
        }
    }
    return nil
}

func (this *Node) StreamDopamine(dopamineAdapterId string, dopamine float64) error {
    if dopamineAdapter, exists := this.dopamineAdapters[dopamineAdapterId]; exists {
        dopamineAdapter.Send(dopamine)
        return nil
    } else {
        return errors.New("Dopamine Adapter Not Found")
    }
}

func (this *Node) StreamNeuronSpikeEvent(neuronClusterId string, stream stream.NeuronSpikeStream) error {
    if _, exists := this.neuronClusters[neuronClusterId]; !exists {
        return errors.New("Neuron Cluster Not Found")
    }
    this.neuronClusters[neuronClusterId].AttachNeuronSpikeObserver(stream)
    // TODO Run in goroutine?
    for {
        time.Sleep(time.Second * 10)
        if neuronCluster, exists := this.neuronClusters[neuronClusterId]; exists {
            if !neuronCluster.HasNeuronSpikeObserver(stream) {
                return nil
            }
        } else {
            return nil
        }
    }
    return nil
}

func (this *Node) StreamNeuronSpikeRelayEvent(synapseClusterId string, stream stream.NeuronSpikeRelayStream) error {
    if _, exists := this.synapseClusters[synapseClusterId]; !exists {
        return errors.New("Synapse Cluster Not Found")
    }

    this.synapseClusters[synapseClusterId].AttachNeuronSpikeRelayObserver(stream)
    // TODO Run in goroutine?
    for {
        time.Sleep(time.Second * 10)
        if synapseCluster, exists := this.synapseClusters[synapseClusterId]; exists {
            if !synapseCluster.HasNeuronSpikeRelayObserver(stream) {
                return nil
            }
        } else {
            return nil
        }
    }
    return nil
}

func (this *Node) StreamSynapseWeightUpdateEvent(synapseClusterId string, stream stream.SynapseWeightUpdateStream) error {
    if _, exists := this.synapseClusters[synapseClusterId]; !exists {
        return errors.New("Synapse Cluster Not Found")
    }

    this.synapseClusters[synapseClusterId].AttachSynapseWeightUpdateObserver(stream)
    // TODO Run in goroutine?
    for {
        time.Sleep(time.Second * 10)
        if synapseCluster, exists := this.synapseClusters[synapseClusterId]; exists {
            if !synapseCluster.HasSynapseWeightUpdateObserver(stream) {
                return nil
            }
        } else {
            return nil
        }
    }
    return nil
}

func NewNode() *Node {
    return &Node{
        neuronClusters: make(map[string]*neuron.NeuronCluster),
        synapseClusters: make(map[string]*synapse.SynapseCluster),
        stdpPreceptors: make(map[string]*preceptor.STDPPreceptor),
        modulatedSTDPPreceptors: make(map[string]*preceptor.ModulatedSTDPPreceptor),
        dopamineAdapters: make(map[string]*dopamine.DopamineAdapter),
        inputAdapters: make(map[string]*input.InputAdapter),
        outputAdapters: make(map[string]*output.OutputAdapter),
        validator: validator.NewNodeValidator(),
    }
}
