package stream

import (
    "../observer"
)

type InputStream interface {
    observer.InputObserver
}

type OutputStream interface {
    observer.OutputObserver
}

type NeuronSpikeStream interface {
    observer.NeuronSpikeObserver
}

type NeuronSpikeRelayStream interface {
    observer.NeuronSpikeRelayObserver
}

type SynapseWeightUpdateStream interface {
    observer.SynapseWeightUpdateObserver
}

type DopamineStream interface {
    observer.DopamineSubject
}
