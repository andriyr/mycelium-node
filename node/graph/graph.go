package graph

type Graph struct {
    x float64
    y float64
}

func (this *Graph) X() float64 {
    return this.x
}

func (this *Graph) SetX(x float64) {
    this.x = x
}

func (this *Graph) Y() float64 {
    return this.y
}

func (this *Graph) SetY(y float64) {
    this.y = y
}

func (this *Graph) SetPosition(x, y float64) {
    this.x = x
    this.y = y
}

func NewGraph(x float64, y float64) *Graph {
    return &Graph{
        x: x,
        y: y,
    }
}
