package output

import (
    "math/rand"
    "time"
    "sync"

    "../neuron"
    "../observer"
    "../graph"

    "github.com/satori/go.uuid"
)

type OutputAdapter struct {
    id string
    name string
    size int
    decodingWindow int
    lastInterval time.Time
    values []float64
    neuronCluster *neuron.NeuronCluster
    neuronIds []string
    *observer.DefaultNeuronSpikeObserver
    *observer.DefaultOutputSubject
    *graph.Graph
    mux sync.Mutex
}

func (this *OutputAdapter) Id() string {
    return this.id
}

func (this *OutputAdapter) Name() string {
    return this.name
}

func (this *OutputAdapter) Size() int {
    return this.size
}

func (this *OutputAdapter) DecodingWindow() int {
    return this.decodingWindow
}

func (this *OutputAdapter) NeuronClusterId() string {
    if this.neuronCluster != nil {
        return this.neuronCluster.Id()
    }
    return ""
}

func (this *OutputAdapter) SetNeuronCluster(neuronCluster *neuron.NeuronCluster) {
    neuronIds := make([]string, 0)
    neuronClusterNeuronIds := neuronCluster.NeuronIds()

    for i := 0; i < this.size; i++ {
        neuronIds = append(neuronIds, neuronClusterNeuronIds[rand.Intn(len(neuronClusterNeuronIds))])
    }

    this.neuronIds = neuronIds
    this.neuronCluster = neuronCluster

    neuronCluster.AttachNeuronSpikeObserver(this)
}

func (this *OutputAdapter) UnsetNeuronCluster() {
    if this.neuronCluster != nil {
        this.neuronCluster.DetachNeuronSpikeObserver(this)
        this.neuronIds = nil
        this.neuronCluster = nil
    }
}

func (this *OutputAdapter) handleNeuronSpikeEvent(event *observer.NeuronSpikeEvent) {
    for idx, neuronId := range this.neuronIds {
        if neuronId == event.NeuronId {
            if this.values[idx] == 0 {
                delta := time.Now().Sub(this.lastInterval)
                this.mux.Lock()
                this.values[idx] = float64(delta)/float64(time.Duration(this.decodingWindow)*time.Millisecond)
                this.mux.Unlock()
            }
        }
    }
}

func (this *OutputAdapter) startDecoding() {
    go func() {
        for {
            values := make([]float64, len(this.values))
            copy(values, this.values)

            this.mux.Lock()
            this.NotifyOutputEvent(&observer.OutputEvent{
                OutputAdapterId: this.Id(),
                Values: values,
            })

            this.lastInterval = time.Now()

            for i := range this.values {
                this.values[i] = 0
            }
            this.mux.Unlock()
            time.Sleep(time.Duration(this.decodingWindow)*time.Millisecond)
        }
    }()
}

func (this *OutputAdapter) listen() {
    this.startDecoding()

    go func() {
        for {
            select {
            case neuronSpikeEvent := <- this.NeuronSpikeChannel():
                this.handleNeuronSpikeEvent(neuronSpikeEvent)
            }
        }
    }()
}

func NewOutputAdapter(name string, size, decodingWindow int, x, y float64) *OutputAdapter {
    outputAdapter := &OutputAdapter{
        id: uuid.Must(uuid.NewV4()).String(),
        name: name,
        size: size,
        decodingWindow: decodingWindow,
        lastInterval: time.Now(),
        values: make([]float64, size),
        neuronCluster: nil,
        neuronIds: nil,
        DefaultNeuronSpikeObserver: observer.NewDefaultNeuronSpikeObserver(),
        DefaultOutputSubject: observer.NewDefaultOutputSubject(),
        Graph: graph.NewGraph(x, y),
    }

    outputAdapter.listen()

    return outputAdapter
}
