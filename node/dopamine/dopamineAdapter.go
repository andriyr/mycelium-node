package dopamine

import (
    "../observer"
    "../graph"

    "github.com/satori/go.uuid"
)

type DopamineAdapter struct {
    id string
    name string
    *observer.DefaultDopamineSubject
    *graph.Graph
}

func (this *DopamineAdapter) Id() string {
    return this.id
}

func (this *DopamineAdapter) Name() string {
    return this.name
}

func (this *DopamineAdapter) Send(dopamine float64) {
    go func(dopamine float64) {
            this.NotifyDopamineEvent(&observer.DopamineEvent{DopamineAdapterId: this.id, Dopamine: dopamine})
    }(dopamine)
}

func NewDopamineAdapter(name string, x, y float64) *DopamineAdapter {
    dopamineAdapter := &DopamineAdapter{
        id: uuid.Must(uuid.NewV4()).String(),
        name: name,
        DefaultDopamineSubject: observer.NewDefaultDopamineSubject(),
        Graph: graph.NewGraph(x, y),
    }

    return dopamineAdapter
}
