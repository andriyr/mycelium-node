package synapse

import (
    "math/rand"

    "../neuron"
    "../observer"
    "../graph"

    "github.com/satori/go.uuid"
)

type SynapseCluster struct {
    id string
    name string
    neuronsCovered int
    maxSynapseWeight float64
    initialSynapseWeight float64
    neuronClusters map[string]*neuron.NeuronCluster
    synapses map[string]*Synapse
    neurons map[string]map[string]*Synapse
    *observer.DefaultNeuronSpikeObserver
    *observer.DefaultSynapseWeightUpdateObserver
    *observer.DefaultNeuronSpikeSubject
    *observer.DefaultNeuronSpikeRelaySubject
    *observer.DefaultSynapseWeightUpdateSubject
    *graph.Graph
}

func (this *SynapseCluster) Id() string {
    return this.id
}

func (this *SynapseCluster) Name() string {
    return this.name
}

func (this *SynapseCluster) NeuronsCovered() int {
    return this.neuronsCovered
}

func (this *SynapseCluster) InitialSynapseWeight() float64 {
    return this.initialSynapseWeight
}

func (this *SynapseCluster) SynapseIds() []string {
    synapseIds := make([]string, 0)

    for synapseId, _ := range this.synapses {
        synapseIds = append(synapseIds, synapseId)
    }

    return synapseIds
}

func (this *SynapseCluster) NeuronClusterIds() []string {
    neuronClusterIds := make([]string, 0)

    for _, neuronCluster := range this.neuronClusters {
        neuronClusterIds = append(neuronClusterIds, neuronCluster.Id())
    }

    return neuronClusterIds
}

// TODO Network crashes when rewiring adapters, detach and re-attach all adapters
func (this *SynapseCluster) AddNeuronCluster(neuronCluster *neuron.NeuronCluster) {
    this.neuronClusters[neuronCluster.Id()] = neuronCluster

    this.wireNeurons()

    neuronCluster.AttachNeuronSpikeObserver(this)
    this.AttachNeuronSpikeRelayObserver(neuronCluster)
}

func (this *SynapseCluster) RemoveNeuronCluster(id string) {
    if neuronCluster, exists := this.neuronClusters[id]; exists {
        neuronCluster.DetachNeuronSpikeObserver(this)
        this.DetachNeuronSpikeRelayObserver(neuronCluster)
        delete(this.neuronClusters, id)
    }
}

func (this *SynapseCluster) addSynapse(synapse *Synapse) {
    if this.neurons[synapse.PreSynapticNeuronId()] == nil {
        this.neurons[synapse.PreSynapticNeuronId()] = make(map[string]*Synapse)
    }

    this.neurons[synapse.PreSynapticNeuronId()][synapse.PostSynapticNeuronId()] = synapse
    this.synapses[synapse.Id()] = synapse
}

func (this *SynapseCluster) wireNeurons() {
    neuronsMap := make(map[string][]string)
    for neuronClusterId := range this.neuronClusters {
        neuronsMap[neuronClusterId] = make([]string, 0)
    }

    neuronClusterIds := make([]string, 0)
    for neuronClusterId := range this.neuronClusters {
        neuronClusterIds = append(neuronClusterIds, neuronClusterId)
    }

    for i := 0; i < this.neuronsCovered; i++ {
        neuronClusterId := neuronClusterIds[rand.Intn(len(neuronClusterIds))]
        neuronIds := this.neuronClusters[neuronClusterId].NeuronIds()
        neuronsMap[neuronClusterId] = append(neuronsMap[neuronClusterId], neuronIds[rand.Intn(len(neuronIds))])
    }

    for preSynapticNeuronClusterId, preSynapticNeuronIds := range neuronsMap {
        for _, preSynapticNeuronId := range preSynapticNeuronIds {
            for postSynapticNeuronClusterId, postSynapticNeuronIds := range neuronsMap {
                for _, postSynapticNeuronId := range postSynapticNeuronIds {
                    if preSynapticNeuronId != postSynapticNeuronId {
                        this.addSynapse(NewSynapse(preSynapticNeuronClusterId, preSynapticNeuronId, postSynapticNeuronClusterId, postSynapticNeuronId, this.maxSynapseWeight, this.initialSynapseWeight, this.onSpikeRelay))
                    }
                }
            }
        }
    }
}

func (this *SynapseCluster) onSpikeRelay(synapseId, neuronClusterId, neuronId string, potential float64) {
    this.NotifyNeuronSpikeRelayEvent(&observer.NeuronSpikeRelayEvent{
        SynapseId: synapseId,
        NeuronClusterId: neuronClusterId,
        NeuronId: neuronId,
        Potential: potential,
    })
}

func (this *SynapseCluster) handleNeuronSpikeEvent(event *observer.NeuronSpikeEvent) {
    if postSynapticNeuronMap, exists := this.neurons[event.NeuronId]; exists {
        this.NotifyNeuronSpikeEvent(event)

        for postSynapticNeuronId := range postSynapticNeuronMap {
            this.neurons[event.NeuronId][postSynapticNeuronId].RelaySpike(event)
        }
    }
}

func (this *SynapseCluster) handleSynapseWeightUpdateEvent(event *observer.SynapseWeightUpdateEvent) {
    this.neurons[event.PreSynapticNeuronId][event.PostSynapticNeuronId].UpdateWeight(event.WeightDelta)
}

func (this *SynapseCluster) listen() {
    go func() {
        neuronSpikeChannel := this.NeuronSpikeChannel()
        synapseWeightUpdateChannel := this.SynapseWeightUpdateChannel()

        for {
            select {
            case neuronSpikeEvent := <- neuronSpikeChannel:
                this.handleNeuronSpikeEvent(neuronSpikeEvent)
            case synapseWeightUpdateEvent := <- synapseWeightUpdateChannel:
                this.handleSynapseWeightUpdateEvent(synapseWeightUpdateEvent)
            }
        }
    }()
}

func NewSynapseCluster(name string, neuronsCovered int, maxSynapseWeight, initialSynapseWeight, x, y float64) *SynapseCluster {
    synapseCluster := &SynapseCluster{
        id: uuid.Must(uuid.NewV4()).String(),
        name: name,
        neuronsCovered: neuronsCovered,
        maxSynapseWeight: maxSynapseWeight,
        initialSynapseWeight: initialSynapseWeight,
        neuronClusters: make(map[string]*neuron.NeuronCluster),
        synapses: make(map[string]*Synapse),
        neurons: make(map[string]map[string]*Synapse),
        DefaultNeuronSpikeObserver: observer.NewDefaultNeuronSpikeObserver(),
        DefaultSynapseWeightUpdateObserver: observer.NewDefaultSynapseWeightUpdateObserver(),
        DefaultNeuronSpikeSubject: observer.NewDefaultNeuronSpikeSubject(),
        DefaultNeuronSpikeRelaySubject: observer.NewDefaultNeuronSpikeRelaySubject(),
        DefaultSynapseWeightUpdateSubject: observer.NewDefaultSynapseWeightUpdateSubject(),
        Graph: graph.NewGraph(x, y),
    }

    synapseCluster.listen()

    return synapseCluster
}
