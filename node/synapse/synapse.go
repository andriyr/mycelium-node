package synapse

import (
    "../observer"

    "github.com/satori/go.uuid"
)

type Synapse struct {
    id string
    preSynapticNeuronClusterId string
    preSynapticNeuronId string
    postSynapticNeuronClusterId string
    postSynapticNeuronId string
    maxWeight float64
    weight float64
    onSpikeRelay func(string, string, string, float64)
}

func (this *Synapse) Id() string {
    return this.id
}

func (this *Synapse) PreSynapticNeuronClusterId() string {
    return this.preSynapticNeuronClusterId
}

func (this *Synapse) PreSynapticNeuronId() string {
    return this.preSynapticNeuronId
}

func (this *Synapse) PostSynapticNeuronClusterId() string {
    return this.postSynapticNeuronClusterId
}

func (this *Synapse) PostSynapticNeuronId() string {
    return this.postSynapticNeuronId
}

func (this *Synapse) UpdateWeight(weightDelta float64) {
    this.weight += weightDelta
    if this.weight < 0 {
        this.weight = 0
    }
    if this.weight > this.maxWeight {
        this.weight = this.maxWeight
    }
}

func (this *Synapse) RelaySpike(event *observer.NeuronSpikeEvent) {
    this.onSpikeRelay(this.id, this.postSynapticNeuronClusterId, this.postSynapticNeuronId, this.weight * event.Potential)
}

func NewSynapse(preSynapticNeuronClusterId, preSynapticNeuronId, postSynapticNeuronClusterId, postSynapticNeuronId string, maxWeight, weight float64, onSpikeRelay func(string, string, string, float64)) *Synapse {
    return &Synapse{
        id: uuid.Must(uuid.NewV4()).String(),
        preSynapticNeuronClusterId: preSynapticNeuronClusterId,
        preSynapticNeuronId: preSynapticNeuronId,
        postSynapticNeuronClusterId: postSynapticNeuronClusterId,
        postSynapticNeuronId: postSynapticNeuronId,
        maxWeight: maxWeight,
        weight: weight,
        onSpikeRelay: onSpikeRelay,
    }
}
