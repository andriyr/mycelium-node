FROM golang:alpine as builder

RUN apk --no-cache add git

ADD ./node /go/src/node
ADD ./api /go/src/api
ADD ./server /go/src/server
ADD ./main.go /go/src/main.go

# TODO generate proto files for go

RUN go get github.com/satori/go.uuid
RUN go get github.com/golang/protobuf/proto
RUN go get google.golang.org/grpc
RUN go get golang.org/x/net/context

RUN go build -o /go/bin/node  /go/src/main.go


FROM alpine

COPY --from=builder /go/bin/node .

EXPOSE 50051

CMD ["/node"]
